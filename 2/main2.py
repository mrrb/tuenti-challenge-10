#!/usr/bin/env python
# Mario Rubio. mrrb.eu

import sys


## Arguemnt parser
'''
To make my life easier, the first argument of the python script is used as the challenge input
and the second one is the file where the result is going to be stored.
'''
input_file, output_file = 'input', 'output'
if len(sys.argv) == 2:
    input_file = sys.argv[1]
elif len(sys.argv) == 3:
    input_file, output_file = sys.argv[1], sys.argv[2]


## Input read
'''
The given challenge file is opened, and all the cases and matches are parsed.

cases = [[case1], [case2], ...]
'''
with open(input_file, 'r') as f:
    n_cases = int(f.readline())

    cases = []
    for i in range(n_cases):
        n_matches = int(f.readline())

        matches = []
        for j in range(n_matches):
            matches.append([int(i)
                            for i in f.readline().rstrip('\n').split(' ')])
        cases.append(matches)


## Main
def move_winner(players, winner, loser):
    '''For the given player list, It will move the winner in front of the loser.'''
    new_index = players.index(loser)
    players.remove(winner)
    players.insert(new_index, winner)


fo = open(output_file, 'w')
for index, case in enumerate(cases):
    players = []

    # case_len = len(case)
    for index2, match in enumerate(case):
        '''For every case, if needed, the winner will be move in front of the loser.'''

        # if index2 % 1000 == 0.:
        #     print(f"#{index2}/{case_len}")

        winner, loser = match[1], match[0]
        if match[2] == 1:
            winner, loser = match[0], match[1]

        if winner not in players:
            players.append(winner)
        winner_index = players.index(winner)

        if loser not in players:
            players.append(loser)
            continue
        else:
            loser_index = players.index(loser)
            
        if winner_index < loser_index:
            move_winner(players, winner, loser)

    # The winner will be in the position 0
    msg = f"Case #{index+1}: {players[0]}\n"
    fo.write(msg)

fo.close()
