#!/usr/bin/env python
# Mario Rubio.

import sys


input_file = 'input'
if len(sys.argv) > 1:
    input_file = sys.argv[1]

with open(input_file, 'r') as f:
    cases = [i.rstrip('\n').split(' ') for i in f.readlines()[1:]]


def chk_case(A, B):
    A, B = A.lower(), B.lower()
    if A == B:
        return '-'

    chk_dict = {'rs': 'R', 'sr': 'R', 'rp': 'P', 'pr': 'P','sp': 'S', 'ps': 'S'}
    return chk_dict.get(''.join([A, B]))


with open("output", 'w') as f:
    for index, case in enumerate(cases):
        result = f"Case #{index+1}: {chk_case(case[0], case[1])}"
        f.write(f"{result}\n")
