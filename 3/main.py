#!/usr/bin/env python
# Mario Rubio. mrrb.eu

import sys
import pickle


## Argument parser
'''
To make my life easier, the first argument of the python script is used as the challenge input
and the second one is the file where the result is going to be stored.

$ ./main.py challenge_input challenge_output book
'''
input_file, output_file, book_file = 'input', 'output', 'book'
if len(sys.argv) == 2:
    input_file = sys.argv[1]
elif len(sys.argv) == 3:
    input_file, output_file = sys.argv[1], sys.argv[2]
elif len(sys.argv) == 4:
    input_file, output_file, book_file = sys.argv[1], sys.argv[2], sys.argv[3]


## Input parse
'''
The given input challenge file is opened, and all the lines are parsed.
'''
cases = []
with open(input_file, 'r') as fo:
    n_cases = int(fo.readline().rstrip('\n'))

    for line in fo.readlines():
        try:
            temp = int(line.rstrip('\n'))
        except:
            temp = line.rstrip('\n')

        cases.append(temp)

## Book read and parse
'''
The given book challenge file is opened, and all the lines are parsed.
'''
valid_letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
                 'l', 'm', 'n', 'ñ', 'o', 'p', 'q', 'r', 's', 't', 'u',
                 'v', 'w', 'x', 'y', 'z', 'á', 'é', 'í', 'ó', 'ú', 'ü']

text = []
words = {}
with open(book_file, 'r') as f:
    for line in f.readlines():
        text.append(line.lower())

        # Remove not valid chars
        for letter in text[-1]:
            if letter not in valid_letters+[' ']:
                text[-1] = text[-1].replace(letter, ' ')

        # Split text and remove spaces
        text[-1] = [word for word in text[-1].split(' ') if word != '']

        # Add words to dict
        for word in text[-1]:
            # Discard not valid words
            if len(word) < 3:
                continue

            # To words dict
            if word in words:
                words[word] += 1
            else:
                words[word] = 1

## Words sort
temp_words = {}
# By UNICODE order
for words_len in set(words.values()):
    temp_words[words_len] = sorted(
        [i for (i, j) in words.items() if j == words_len])

# Then, by word len
temp_words = dict(sorted(temp_words.items(), key=lambda x: x[0], reverse=True))

# Re-create the words dict with the sorted data
words = {}
for word_len, words_list in temp_words.items():
    temp = {word: word_len for word in words_list}
    words = {**words, **temp}

# Crete rank dict
words_list = list(words.keys())
words_rank = dict(zip(words_list, range(1, len(words))))


## Pickle data for future use (if needed)
with open('words.pkl', 'wb') as fo:
    pickle.dump(words, fo)


## Main
with open(output_file, 'w') as fo:
    for index, case in enumerate(cases):
        if isinstance(case, int):
            # The given case is a number, so the script has to find the word and its total count
            word = words_list[case-1]
            msg = f"Case #{index+1}: {word} {words[word]}\n"
        else:
            # The given case is a word, so the script has to find its total count and rank
            msg = f"Case #{index+1}: {words[case]} #{words_rank[case]}\n"

        fo.write(msg)
